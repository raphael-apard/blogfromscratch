<?php

$email = $fistname = $lastname ='';

if(isset($_POST['signup'])){

    //check empty fields
    if (empty($_POST['firstname']) ||
    empty($_POST['lastname']) ||
    empty($_POST['email']) ||
    empty($_POST['password'])) {
    
    die('Please fill all required fields!');
    }
    else {
        $email = $_POST['email'];
        $password = $_POST['password'];
        $firstname = $_POST['firstname'];
        $lastname = $_POST['lastname'];
    }
   

    $errors = ['email'=>'','password'=>'','firstname'=>'','lastname'=>''];


    // check validate email
    if(!filter_var($email,FILTER_VALIDATE_EMAIL)){
        $errors['email']= "Email must be a valid email address.";
    }
      
    //check validate password
    if(!preg_match("#.*^(?=.{8,20})(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*\W).*$#", $password )){
        $errors['password'] = "Password must be between 8-20 characters including at least 1 uppercase,1 lowercase,1 symbole and 1 number .";
    }
    
    //check firstname lastname
    if (!preg_match("/^[a-zA-Z]+$/", $_POST["firstname"])){

        $errors['firstname'] = "Please enter a valid first name";          
    }
    if (!preg_match("/^[a-zA-Z]+$/", $_POST["lastname"])){

        $errors['lastname'] = "Please enter a valid last name";          
    }

    if(!array_filter($errors)){
        include('../config/connect.php');


        $email = mysqli_real_escape_string($conn,$_POST['email']);
        
        $password = mysqli_real_escape_string($conn,$_POST['password']);
        
        $firstname = mysqli_real_escape_string($conn,$_POST['firstname']);
        
        $lastname = mysqli_real_escape_string($conn,$_POST['lastname']);
        // $password = hash('sha512',$password);
        
        $query = "INSERT INTO authors (email,password,firstname,lastname) VALUES ('$email','$password','$firstname','$lastname') ";
        
        $result = mysqli_query($conn,$query);

        if($result)
        {
            header("location:./login.php");
        
        }
        else
        {
        echo "Error" . mysqli_error($conn);
        
        }
        
    }
 
    
    
    
    

    
    
}



include '../static/header.php';

?>
<div class="container">
<form action="signup.php" method="post">
 
 <h2>Sign Up</h2>
 <div class="row">
    <div class="col-25">
 <label for="email">Email</label>
 </div>
    <div class="col-75">
 <input type="text" name="email" value="<?php echo htmlspecialchars($email) ?>">
<div> <?php echo $errors['email'] ;?> </div>
</div>
  </div>

<div class="row">
    <div class="col-25">
 <label for="password">Password</label>
 </div>
    <div class="col-75">
 <input type="password" name="password">
 <div> <?php echo $errors['password'] ;?> </div>
  </div>
  </div>

 <div class="row">
    <div class="col-25">
 <label for="firstname">Firstname</label>
 </div>
    <div class="col-75">
 <input type="text" name="firstname" value="<?php echo htmlspecialchars($firstname) ?>">
 <div> <?php echo $errors['firstname'] ;?> </div>
  </div>
  </div>

 <div class="row">
    <div class="col-25">
 <label for="lastname">Lastname</Label>
 </div>
    <div class="col-75">
 <input type="text" name="lastname" value="<?php echo htmlspecialchars($lastname)?>">   
 <div> <?php echo $errors['lastname'] ;?> </div>
  </div>
  </div>

 <div class="row">
 <input type="submit" name="signup" value="signup">
   
    </div>
 </form>
 
</div>
 


 <?php

include '../static/footer.php';
?>