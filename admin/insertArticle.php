<?php

/* on affiche  les erreurs, */
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

session_start();

if(isset($_SESSION['author_id'])){
    

    if(isset($_POST['submit'])){

        $title=$_POST['title'];
        $content=$_POST['content'];
        $image_url=$_POST['title'];
        $reading_time=$_POST['reading_time'];

        include('../config/connect.php');

        // not so clear about the syntax here with the '".$variable ."'
        $query ="INSERT INTO articles (title,content,image_url,author_id,reading_time)
        VALUES ('".$title."','".$content."','".$image_url."','".$_SESSION['author_id']."','".$reading_time."')";

        $result = mysqli_query($conn,$query);
        if($result)
        {
            echo "Thanks for submitting your article!";
            
        
        }
        else
        {
        echo "Error" . mysqli_error($conn);
        
        }
       
    }    
}


include '../static/header.php';


?>


<div class="container">
<form action="insertArticle.php" method="post">
 
 <h2>write a new article</h2>

 <div class="row">
    <div class="col-25">
         <label>Title</label>
    </div>
    <div class="col-75">
        <input type="text" name="title">
    </div>
  </div>

  <div class="row">
    <div class="col-25">
        <label>Content</label>
     </div>
    <div class="col-75">
    <textarea name="content" placeholder="Write something.." style="height:400px"></textarea>
    
    </div>
  </div>

  <div class="row">
    <div class="col-25">

         <label>Image_url</label>
     </div>
    <div class="col-75">
        <input type="text" name="image_url">
    </div>
  </div>

  <div class="row">
    <div class="col-25">
        <label>Reading_time</label>
    </div>
    <div class="col-75">
         <input type="text" name="reading_time">
    </div>
  </div>

  <div class="row">
   
        <input type="submit" name="submit" value="submit">
    
    </div>
 </form>
 
</div>
 

 <?php

include '../static/footer.php';
?>